def boucle_for():
    age = 10  # Initialize the 'age' variable with a value
    for i in range(age):  # Use 'range' to create a sequence from 0 to 'age - 1'
        print(i + 1)  # Increment 'i' by 1 and print it

boucle_for()