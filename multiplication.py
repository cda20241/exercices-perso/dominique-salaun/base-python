def multiplication(input1, input2):
    try:
        input1 = float(input1)
        input2 = float(input2)
        if input2 == 0:
            return "Division by zero is not allowed."
        else:
            result = input1 * input2
            return result
    except ValueError:
        return "Invalid input. Please enter valid numeric values."

# Get user input for input1 and input2
input1 = input("Enter the first number: ")
input2 = input("Enter the second number: ")

# Call the division function with user inputs
result = multiplication(input1, input2)

# Display the result
print("Result:", result)