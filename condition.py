def condition():
    while True:
        valeur = input("Entrez un nombre : ")

        try:
            valeur = int(valeur)
        except ValueError:
            print("Veuillez entrer un nombre valide.")
            continue

        if valeur > 15:
            print("La valeur est supérieure à 15.")
        else:
            print("La valeur est inférieure ou égale à 15.")

        break  # Exit the loop after processing the input


condition()